let gods = require('./arquivo_exercicio_4.js')

// Letra A:

// for(i=0; i<gods.length; i++){
//             console.log((gods[i].name), (gods[i].features.length))
// }

//Letra B:

// for(i=0; gods.length; i++){
//     if(gods[i].roles == "Mid"){
//         console.log(gods[i])
//     }
// }

//Letra C:

// console.log(gods.sort((a,b)=>a.pantheon<b.pantheon ? -1 : 0))

//Letra D:

// for(i=0; i<gods.length -1; i++){
//     console.log(gods[i].name + ' ('+gods[i].class + ')')
// }
